package net.gamechaser.samaritanDiscord;

import java.util.ArrayList;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.gamechaser.samaritanDiscord.apiTools.Trello;
import net.gamechaser.samaritanDiscord.helpers.DiscordHelper;
import net.gamechaser.samaritanDiscord.helpers.HTTPHelper;
import net.gamechaser.samaritanDiscord.helpers.WordHelper;

public class TrelloThread implements Runnable
{
	String channelID;
	String lastCheck;

	public void run() 
	{
		while (true) 
		{
			try { Thread.sleep(300000); } catch (InterruptedException e) { e.printStackTrace(); }
			try {
				if(getTrello(channelID, lastCheck) == 1) {
					this.lastCheck = Trello.generateTime();
					Trello.recordEndTime();
				}
			} catch (Exception e1) { e1.printStackTrace(); }
		}
	}
	
	private static int getTrello(String channelID, String date) throws Exception 
	{
		String[] trelloBoardIDs = {"6BwRMiPw", "gWpe3lzV", "NlQNuSOY", "vd6Sx16X"};
		String[] trelloBoardNames = {"[Roadmap]", "[Art]", "[Programming]", "[Bug Report]"};
		ArrayList<String> messagesList = new ArrayList<String>();
		int boardsWithUpdates = 0;
		boolean areUnlisted = false;
		int unlistedRoadmapUpdates = 0;
		int unlistedArtUpdates = 0;
		int unlistedProgrammingUpdates = 0;
		int unlistedBugReportUpdates = 0;
	
		for (int i = 0; i<trelloBoardIDs.length; i++) 
		{
			String trelloJSON = HTTPHelper.download("https://api.trello.com/1/boards/" + trelloBoardIDs[i] + "/actions?filter=addChecklistToCard,updateCheckItemStateOnCard,createCard,updateCard,addAttachmentToCard,deleteAttachmentFromCard,addMemberToCard,removeMemberFromCard,moveCardFromBoard,moveCardToBoard,emailCard&since=" + date);
			if (trelloJSON.equals("Connection Failed")) 
			{
				return 0;
			}
		
			JSONParser parser = new JSONParser();
			JSONArray mainArray = (JSONArray) parser.parse(trelloJSON);
			int arrayCount = (mainArray.size() >= 5) ? 5 : mainArray.size();
			if(mainArray.size() == 0) { continue; }
			boardsWithUpdates++;

			for (int x = 0; x < arrayCount; x++) 
			{
				JSONObject mainObject = (JSONObject) mainArray.get(x);
				JSONObject updateData = (JSONObject) mainObject.get("data");
				JSONObject userData = (JSONObject) mainObject.get("memberCreator");
				JSONObject cardData = (JSONObject) updateData.get("card");
				
				String cardName = cardData.get("name").toString();
				String userName = userData.get("fullName").toString();
				String updateType = mainObject.get("type").toString();
				
				switch (updateType) 
				{
					case "updateCheckItemStateOnCard":
						JSONObject checkItemData = (JSONObject) updateData.get("checkItem");
						
						String checkItemName = checkItemData.get("name").toString();
						String checkItemState = checkItemData.get("state").toString();
						String responseState = checkItemState.equals("complete") ? "completed" : "set state to incomplete";
						
						if (checkItemName.startsWith("https")) 
						{
							Pattern pattern = Pattern.compile("https:\\/\\/trello.com\\/c\\/(.*?)\\/");
							Matcher matcher = pattern.matcher(checkItemName);
							
							if (matcher.find()) 
							{
								if (matcher.group(1) != null) 
								{
									String checkItemCardJSON = HTTPHelper.download("https://api.trello.com/1/card/" + matcher.group(1));
									if(!checkItemCardJSON.equals("Connection Failed")) 
									{
										JSONParser checkItemParser = new JSONParser();
										JSONObject checkItemCardData = (JSONObject) checkItemParser.parse(checkItemCardJSON);
										checkItemName = checkItemCardData.get("name").toString();
									}
								}
							}
						}
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **" + responseState + "** \\\"" + checkItemName + "\\\" on \\\"" + cardName + "\\\".");
						break;
					case "addChecklistToCard":
						JSONObject checklistData = (JSONObject) updateData.get("checklist");
						String checklistName = checklistData.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **added** \\\"" + checklistName + "\\\" to \\\"" + cardName + "\\\".");
						break;
					case "createCard":
						JSONObject listData = (JSONObject) updateData.get("list");
						String listName = listData.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **created** \\\"" + cardName + "\\\" and added to \\\"" + listName + "\\\".");
						break;
					case "emailCard":
						JSONObject emailListData = (JSONObject) updateData.get("list");
						String emailListName = emailListData.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **created** \\\"" + cardName + "\\\" and added to \\\"" + emailListName + "\\\".");
						break;
					case "updateCard":
						JSONObject oldData = (JSONObject) updateData.get("old");
						@SuppressWarnings("unchecked")
						Set<String> oldKeys = oldData.keySet();
						for (String s : oldKeys) 
						{
							switch (s) 
							{
								case "due":
									messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **updated due date** of \\\"" + cardName + "\\\".");
									break;
								case "idList":
									String idListJSON = HTTPHelper.download("https://api.trello.com/1/lists/" + cardData.get("idList").toString() + "/?key=f09c54fe23f49b87d6bdc2a5880d8988");
									if(!idListJSON.equals("Connection Failed")) 
									{
										JSONParser idListParser = new JSONParser();
										JSONObject idListData = (JSONObject) idListParser.parse(idListJSON);
										String idListName = idListData.get("name").toString();
										
										messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **moved** \\\"" + cardName + "\\\" to \\\"" + idListName + "\\\".");
									} 
									else 
									{
										continue;
									}
									break;
								case "name":
									String oldName = oldData.get("name").toString();
									String newName = cardData.get("name").toString();
									messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **renamed** \\\"" + oldName + "\\\" to \\\"" + newName + "\\\".");
									break;
								case "desc":
									messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **updated the description** of \\\"" + cardName + "\\\".");
									break;
								case "closed":
									boolean oldValue = (boolean) oldData.get("closed");
									String action = oldValue ? "un-archived" : "archived";
									messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **" + action + "** \\\"" + cardName + "\\\".");
									break;
								default:
									break;
							}
						}
						break;
					case "addAttachmentToCard":
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **added attachment(s)** to \\\"" + cardName + "\\\".");
						break;
					case "deleteAttachmentFromCard":
						JSONObject attachmentData = (JSONObject) updateData.get("attachment");
						String attachmentName = attachmentData.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **deleted attachment** \\\"" + attachmentName + "\\\" from \\\"" + cardName + "\\\".");
						break;
					case "addMemberToCard":
						JSONObject addMemberData = (JSONObject) mainObject.get("member");
						String addMemberName = addMemberData.get("fullName").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **added** \\\"" + addMemberName + "\\\" to \\\"" + cardName + "\\\".");
						break;
					case "removeMemberFromCard":
						JSONObject removeMemberData = (JSONObject) mainObject.get("member");
						String removeMemberName = removeMemberData.get("fullName").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **removed** \\\"" + removeMemberName + "\\\" from \\\"" + cardName + "\\\".");
						break;
					case "moveCardFromBoard":
						JSONObject boardTarget = (JSONObject) updateData.get("boardTarget");
						String boardTargetName = boardTarget.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **moved** \\\"" + cardName + "\\\" to external board \\\"" + boardTargetName + "\\\".");
						break;
					case "moveCardtoBoard":
						JSONObject boardSource = (JSONObject) updateData.get("boardSource");
						JSONObject incomingListData = (JSONObject) updateData.get("list");
						
						String boardSourceName = boardSource.get("name").toString();
						String incomingListName = incomingListData.get("name").toString();
						messagesList.add("``[Trello]" + trelloBoardNames[i] + "`` " + userName + " **moved** \\\"" + cardName + "\\\" to \\\"" + incomingListName + "\\\" from board \\\"" + boardSourceName + "\\\".");
						break;
					default:
						break;
				}
			}
			if (mainArray.size() > 5) 
			{
				int unlisted = mainArray.size() - 5;
				switch (trelloBoardIDs[i]) 
				{
					case "6BwRMiPw":
						unlistedRoadmapUpdates = unlisted;
						break;
					case "gWpe3lzV":
						unlistedArtUpdates = unlisted;
						break;
					case "NlQNuSOY":
						unlistedProgrammingUpdates = unlisted;
						break;
					case "vd6Sx16X":
						unlistedBugReportUpdates = unlisted;
						break;
				}
				areUnlisted = true;
			}
		}
		
		if (areUnlisted) 
		{
			String updateCorrect = "";
			StringBuilder unlistedMessageBuilder = new StringBuilder();
			unlistedMessageBuilder.append("[Trello] ");
			if (unlistedRoadmapUpdates > 0) 
			{
				updateCorrect = unlistedRoadmapUpdates > 1 ? "Updates" : "Update";
				unlistedMessageBuilder.append(unlistedRoadmapUpdates + " Roadmap " + updateCorrect + ", ");
			}
			if (unlistedArtUpdates > 0) 
			{
				updateCorrect = unlistedArtUpdates > 1 ? "Updates" : "Update";
				unlistedMessageBuilder.append(unlistedArtUpdates + " Art " + updateCorrect + ", ");
			}
			if (unlistedProgrammingUpdates > 0) 
			{
				updateCorrect = unlistedProgrammingUpdates > 1 ? "Updates" : "Update";
				unlistedMessageBuilder.append(unlistedProgrammingUpdates + " Programming " + updateCorrect + ", ");

			}
			if (unlistedBugReportUpdates > 0) 
			{
				updateCorrect = unlistedBugReportUpdates > 1 ? "Updates" : "Update";
				unlistedMessageBuilder.append(unlistedBugReportUpdates + " Bug Report " + updateCorrect + ", ");
			}
			messagesList.add(WordHelper.endList(unlistedMessageBuilder.toString(), false) + " were not listed.");
		}
		
		if(boardsWithUpdates > 0)
		{
			String[] trelloResult = new String[messagesList.size()];
			trelloResult = messagesList.toArray(trelloResult);
			DiscordHelper.write(channelID, trelloResult);
		}
		return 1;
	}

	public TrelloThread(String channelID, String lastCheck) 
	{
		this.channelID = channelID;
		this.lastCheck = lastCheck;
	}
}
