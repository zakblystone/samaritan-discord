package net.gamechaser.samaritanDiscord;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import net.gamechaser.samaritanDiscord.apiTools.Twitch;

public class TwitchThread implements Runnable {

	String channelID;
	Map<Integer, String> names;
	Map<Integer, String> urls;
	ArrayList<Integer> knownStreaming;
	
	public void run() {
		while(true) {
			try { Thread.sleep(30000); } catch (InterruptedException e) { e.printStackTrace(); }
			try { this.knownStreaming = Twitch.check(channelID, names, urls, knownStreaming); } catch (Exception e) { e.printStackTrace(); }
		}
	}
	
	public TwitchThread(String channelID, Map<Integer, String> names, Map<Integer, String> urls) throws ParseException, IOException, InterruptedException, org.json.simple.parser.ParseException {

		this.channelID = channelID;
		this.names = names;
		this.urls = urls;
		
		// Build up list of people we know we are streaming initially.
		this.knownStreaming = Twitch.check(channelID, names, urls);
	}
}
