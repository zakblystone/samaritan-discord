package net.gamechaser.samaritanDiscord.commandParser;

public class CommandResults {

	public String[] output;
	
	public String toString()
	{
		if (output == null) return "No Result";
		String out = "";
		
		for (int i=0; i<output.length; ++i)
		{
			out += output[i] + ", ";
		}
		
		return out;
	}
}
