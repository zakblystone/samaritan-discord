package net.gamechaser.samaritanDiscord.commandParser;

import java.io.IOException;

import net.gamechaser.samaritanDiscord.helpers.DiscordHelper;

//This class is a hack, just prints stuff to output
public class PrintCommand implements IChatCommand {

	private String description;
	private String[] output;
	public static String channelID;
	
	public PrintCommand(String description, String[] output)
	{
		this.description = description;
		this.output = output;
	}
	
	@Override
	public boolean execute(CommandPacket command, CommandResults results) {
		try {
			DiscordHelper.write(channelID, output);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String getDescriptionString() {
		// TODO Auto-generated method stub
		return description;
	}

}
