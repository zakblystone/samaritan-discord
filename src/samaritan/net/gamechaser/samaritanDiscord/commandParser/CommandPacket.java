package net.gamechaser.samaritanDiscord.commandParser;

import java.util.ArrayList;

public class CommandPacket {

	private String command;
	private ArrayList<String> arguments;
	
	public CommandPacket()
	{
		arguments = new ArrayList<String>();
	}
	
	//Get list of arguments
	public ArrayList<String> getArgs()
	{
		return arguments;
	}
	
	//Get invoking command
	public String getCommand()
	{
		return command;
	}
	
	//Get concatenated argument string
	public String getArgumentString()
	{
		String out = "";
		for (String arg : arguments)
		{
			out += arg + " ";
		}
		if (out.length() > 0) out = out.substring(0, out.length()-1);
		return out;
	}
	
	//Get single argument
	public String getArg(int i)
	{
		return arguments.get(i);
	}
	
	//Number of arguments
	public int numArgs()
	{
		return arguments.size();
	}
	
	//Parse command string into separate elements and create a command packet
	public static CommandPacket parse(String command)
	{
		CommandPacket parsed = new CommandPacket();
		String[] elements = command.split(" ");
		
		//Strip off the '!' and store in the 'command' variable of the packet
		parsed.command = elements[0].substring(1);
	
		//Add space delimited elements into the remaining arguments
		for (int i=1; i<elements.length; ++i)
		{
			parsed.arguments.add(elements[i]);
		}
		
		return parsed;
	}
}
