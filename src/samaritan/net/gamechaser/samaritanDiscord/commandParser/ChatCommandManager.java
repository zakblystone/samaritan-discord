package net.gamechaser.samaritanDiscord.commandParser;

import java.util.HashMap;

public class ChatCommandManager {
	
	//global chat command manager instance
	private static ChatCommandManager instance;
	
	//list of available chat commands
	private HashMap<String, IChatCommand> commandList;
	
	//reults from last executed command
	private CommandResults commandResults;
	
	//private constructor to prevent explicit instantiation
	protected ChatCommandManager() 
	{
		commandList = new HashMap<String, IChatCommand>();
		commandResults = null;
	}
	
	
	//Adds a chat command to the list of available commands
	public void addChatCommand(String key, IChatCommand command)
	{
		commandList.put(key, command);
	}
	
	public CommandResults getCommandResults()
	{
		return commandResults;
	}
	
	//Executes a command from a given string, returns false if the command failed to parse or execute
	public boolean executeCommandString(String command)
	{
		//Reject invalid commands
		if (!validateCommand(command)) return false;
		
		//Try to parse and execute the command
		try
		{
			//Get a parsed command packet
			CommandPacket packet = CommandPacket.parse(command);
			
			//Grab the desired command from the command list
			IChatCommand cmd = commandList.get(packet.getCommand());
			if (cmd != null)
			{
				//Results get stored in this
				commandResults = new CommandResults();
				
				//Execute the command if it was found
				return cmd.execute(packet, commandResults);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
		return false;
	}
	
	//Checks to see if a command string is valid
	public boolean validateCommand(String command)
	{
		//If it doesn't have a '!' it's not a command
		if (command.startsWith("!") == false) return false;
		
		//Otherwise, let's give it a shot
		return true;
	}
	
	//Gets the global chat command manager. Constructs once as a singleton
	public static ChatCommandManager getChatCommandManager()
	{
		if (instance == null)
		{
			instance = new ChatCommandManager();
		}
		return instance;
	}
}
