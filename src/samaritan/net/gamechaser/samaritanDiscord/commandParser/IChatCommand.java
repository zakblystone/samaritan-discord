package net.gamechaser.samaritanDiscord.commandParser;

public interface IChatCommand {

	
	public boolean execute(CommandPacket command, CommandResults results);
	public String getDescriptionString();
	
}
