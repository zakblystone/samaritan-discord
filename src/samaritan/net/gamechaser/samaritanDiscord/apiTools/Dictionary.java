package net.gamechaser.samaritanDiscord.apiTools;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import net.gamechaser.samaritanDiscord.Credentials;
import net.gamechaser.samaritanDiscord.commandParser.CommandPacket;
import net.gamechaser.samaritanDiscord.commandParser.CommandResults;
import net.gamechaser.samaritanDiscord.commandParser.IChatCommand;
import net.gamechaser.samaritanDiscord.helpers.WordHelper;

public class Dictionary implements IChatCommand
{
	
	public static String[] lookup(String word) throws SAXException, IOException, ParserConfigurationException 
	{
		String urlQuery = URLEncoder.encode(word, "UTF-8");
		String[] WordFailed = {"``[Dictionary]`` Word Not Found."};
		String[] ConnectionFailed = {"``[Dictionary]`` Connection Error. Please try again in a few moments."};
		
		String suggestionList = "";
		String queryXML = "http://www.dictionaryapi.com/api/v1/references/collegiate/xml/" + urlQuery + "?key=" + Credentials.getDictionaryAPI();
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document;
		
		try { document = builder.parse(new URL(queryXML).openStream()); } catch (Exception e) { return ConnectionFailed; }

		NodeList suggestionNodeList = document.getElementsByTagName("suggestion");
		NodeList entryNodeList = document.getElementsByTagName("entry");
		
		if (suggestionNodeList.getLength() > 0) 
		{
			for (int i=0; i < suggestionNodeList.getLength(); i++) 
			{
				Node node = suggestionNodeList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) 
				{
					Element element = (Element) node;
					suggestionList = suggestionList + element.getTextContent() + ", ";
					continue;
				}
			}
			String[] response = new String[] {"``[Dictionary]`` Word Not Found.", "``[Dictionary]`` Suggested Words: " + WordHelper.endList(suggestionList, true)};
			return response;
		} 
		else if (entryNodeList.getLength() > 0) 
		{
			Node node = entryNodeList.item(0);
			if (node.getNodeType() == Node.ELEMENT_NODE) 
			{
				Element element = (Element) node;
				
				String resultword = element.getElementsByTagName("ew").item(0).getTextContent();
				
				NodeList partofspeech = element.getElementsByTagName("fl"); 
				String resultnamingword = partofspeech.getLength() > 0 ? element.getElementsByTagName("fl").item(0).getTextContent() : "N/A";
				String resultdef = element.getElementsByTagName("dt").item(0).getTextContent();
				
				String stringResponse = "``[Dictionary]`` " + WordHelper.capitalizeFirst(resultword) + " | " + WordHelper.capitalizeEvery(resultnamingword) + " | " + WordHelper.makeSentence(resultdef.substring(1));
				String[] response = new String[] {stringResponse};
				return response;
			}
		}
		return WordFailed;
	}

	@Override
	public boolean execute(CommandPacket command, CommandResults results) {
		if (command.numArgs() == 0) return false; //need a query
		try
		{
			//Search using the concatenated argument string
			results.output = lookup(command.getArgumentString());
			return true;
		} 
		catch (Exception e)
		{
			results.output = new String[] {"Failed to execute dictionary lookup: ", e.getMessage()};
		}
		return false;
	}

	@Override
	public String getDescriptionString() {
		// TODO Auto-generated method stub
		return "Lookup a word in the dictionary";
	}
}
