package net.gamechaser.samaritanDiscord;

import net.gamechaser.samaritanDiscord.apiTools.Dictionary;
import net.gamechaser.samaritanDiscord.apiTools.GoogleImageSearch;
import net.gamechaser.samaritanDiscord.apiTools.GoogleSearch;
import net.gamechaser.samaritanDiscord.apiTools.Trello;
import net.gamechaser.samaritanDiscord.apiTools.Twitch;
import net.gamechaser.samaritanDiscord.apiTools.Weather;
import net.gamechaser.samaritanDiscord.apiTools.YouTubeSearch;
import net.gamechaser.samaritanDiscord.commandParser.ChatCommandManager;
import net.gamechaser.samaritanDiscord.commandParser.CommandPacket;
import net.gamechaser.samaritanDiscord.commandParser.CommandResults;
import net.gamechaser.samaritanDiscord.commandParser.IChatCommand;
import net.gamechaser.samaritanDiscord.commandParser.PrintCommand;
import net.gamechaser.samaritanDiscord.helpers.DiscordHelper;
import sx.blah.discord.DiscordClient;
import sx.blah.discord.handle.*;
import sx.blah.discord.handle.impl.events.*;
import sx.blah.discord.handle.obj.Message;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.parser.ParseException;

public class Samaritan implements IListener<MessageReceivedEvent>
{
	//GLOBALS
	
	private String[] indiegogoOutput = {
	"Tower Unite Indiegogo Final Total:", 
	"$73,633 USD raised by 2,051 people.", 
	"https://www.indiegogo.com/projects/tower-unite"
	};
	
	private String[] infoOutput = {
	"``Samaritan Version:`` 0.0.2",
	"``System Administrator:`` Caboose700",
	"``Primary    Mandate:`` Detect Trello Updates", 
	"``Secondary  Mandate:`` Detect Pixeltail Developer Twitch Streams",
	"``Tertiary   Mandate:`` Provide Tools for Discord Users"
	};
	
	private String[] helpMessage = {
	"Operational Directives:",
	"-----------------------",
	"``!igg``    (Provides Indiegogo Information for Tower Unite)",
	"``!twitch`` (Checks to see if any Pixeltail Developers are Streaming)",
	"``!info-samaritan`` (Displays Information about Samaritan)",
	"``!g`` (Google Search)        | Syntax: !g  *SEARCH TERM HERE*",
	"``!gi`` (Google Image Search) | Syntax: !gi *SEARCH TERM HERE*",
	"``!yt`` (Youtube Search)      | Syntax: !yt *SEARCH TERM HERE*",
	"``!w`` (Weather Search)       | Syntax: !w  *CITY,COUNTRY*",
	"``!define`` (Dictionary)      | Syntax: !define *WORD HERE*"
	};

	private Map<Integer, String> TwitchNames = new HashMap<Integer, String>();
	private Map<Integer, String> TwitchURLs = new HashMap<Integer, String>();
	private String channelID;
	private Map <Long, Long> UserCooldown = new HashMap<Long, Long>();
	
	public Samaritan()
	{
		String[] indiegogo = {
		"Tower Unite Indiegogo Final Total:", 
		"$73,633 USD raised by 2,051 people.", 
		"https://www.indiegogo.com/projects/tower-unite"
		};
		
		String[] info = {
		"``Samaritan Version:`` 0.0.2",
		"``System Administrator:`` Caboose700",
		"``Primary    Mandate:`` Detect Trello Updates", 
		"``Secondary  Mandate:`` Detect Pixeltail Developer Twitch Streams",
		"``Tertiary   Mandate:`` Provide Tools for Discord Users"
		};
		
		String[] helpMessage = {
		"Operational Directives:",
		"-----------------------",
		"``!igg``    (Provides Indiegogo Information for Tower Unite)",
		"``!twitch`` (Checks to see if any Pixeltail Developers are Streaming)",
		"``!info-samaritan`` (Displays Information about Samaritan)",
		"``!g`` (Google Search)        | Syntax: !g  *SEARCH TERM HERE*",
		"``!gi`` (Google Image Search) | Syntax: !gi *SEARCH TERM HERE*",
		"``!yt`` (Youtube Search)      | Syntax: !yt *SEARCH TERM HERE*",
		"``!w`` (Weather Search)       | Syntax: !w  *CITY,COUNTRY*",
		"``!define`` (Dictionary)      | Syntax: !define *WORD HERE*"
		};
		
		ArrayList<String> LoginInfo = Credentials.DiscordCredentials();
		String Username = LoginInfo.get(0);
		String Password = LoginInfo.get(1);
		
		channelID = LoginInfo.get(2);
		
		//Pass to print command
		PrintCommand.channelID = channelID;
		
		ChatCommandManager manager = ChatCommandManager.getChatCommandManager();

		manager.addChatCommand("igg", new PrintCommand("Prints indiegogo status", indiegogoOutput));
		manager.addChatCommand("info-samaritan", new PrintCommand("Prints info about samaritan", infoOutput));
		
		//TODO, make help enumerate commands and print descriptions
		manager.addChatCommand("help", new PrintCommand("Prints help on using samaritan", helpMessage));
		manager.addChatCommand("twitch", new TwitchCommand());
		manager.addChatCommand("define", new Dictionary());
		manager.addChatCommand("g", new GoogleSearch());
		manager.addChatCommand("gi", new GoogleImageSearch());
		manager.addChatCommand("yt", new YouTubeSearch());
		manager.addChatCommand("w", new Weather());

		//Can't login, we're done
		if (!Login(Username, Password)) {
			System.err.println("Unable to log into discord");
			return;
		}
		
		InitStaticData();
		if (!InitThreads())
		{
			System.err.println("Failed to start some of the threads.");
		}
		
		DiscordClient.get().getDispatcher().registerListener(this);
		
		System.out.println("HI");
	}
	
	boolean Login(String username, String password)
	{
		System.out.println("User: " + username);
		System.out.println("Pass: " + password);
		
		try {
			DiscordClient.get().login(username, password);
			DiscordHelper.write(Credentials.getDebugChannel(), "Samaritan Online");
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(DiscordClient.get().getToken());
		
		return false;
	}
	
	boolean InitThreads()
	{
		Thread tTwitchThread;
		try {
			tTwitchThread = new Thread(new TwitchThread(channelID, TwitchNames, TwitchURLs));
			tTwitchThread.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			Thread tTrelloThread = new Thread(new TrelloThread(channelID, Trello.getEndTime()));
			tTrelloThread.start();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	void InitStaticData()
	{
		TwitchNames.put(88752545, "Pixeltail Games");
		TwitchNames.put(2185137,  "MacDGuy");
		TwitchNames.put(6854939,  "Foohy");
		TwitchNames.put(32039860, "Matt");
		TwitchNames.put(22899421, "Lifeless");
		TwitchNames.put(46510960, "Zak");
		
		TwitchURLs.put(88752545, "http://www.twitch.tv/pixeltailgames");
		TwitchURLs.put(2185137,  "http://www.twitch.tv/macdguy");
		TwitchURLs.put(6854939,  "http://www.twitch.tv/foohy");
		TwitchURLs.put(32039860, "http://www.twitch.tv/gamedev11");
		TwitchURLs.put(22899421, "http://www.twitch.tv/lifeless2011");
		TwitchURLs.put(46510960, "http://www.twitch.tv/zakblystone");
	}
	
	@Override
	public void receive(MessageReceivedEvent event) {
		Message m = event.getMessage();
		String messageContent = m.getContent();
		long authorID = Long.parseLong(m.getAuthor().getID());
		
		ChatCommandManager manager = ChatCommandManager.getChatCommandManager();
		
		if (manager.validateCommand(messageContent)) {
			if (UserCooldown.get(authorID) != null)
			{
				long userTimestamp = UserCooldown.get(authorID);
				if (System.currentTimeMillis() - userTimestamp > 30000) 
				{
					UserCooldown.put(authorID, System.currentTimeMillis());
					System.out.println(authorID + " was allowed to speak.");
				}
				else
				{
					System.out.println(authorID + " can't speak yet.");
					return;
				}
			}
			else
			{
				UserCooldown.put(authorID, System.currentTimeMillis());
				System.out.println(authorID + " has been added to the list.");
			}
			
			boolean success = manager.executeCommandString(messageContent);
			CommandResults result = manager.getCommandResults();
			
			if (result != null && result.output != null)
			{
				try {
					DiscordHelper.write(channelID, manager.getCommandResults().output);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		// Suspend (will use command manager once users are supported)
		if (m.getContent().startsWith("!suspend") && m.getAuthor().getID().equals("93949605548392448")) 
		{
			try 
			{ 
				DiscordHelper.write(Credentials.getDebugChannel(), "Samaritan Offline"); 
			} 
			catch (IOException e) { e.printStackTrace(); }
			
			try 
			{ 
				// Record Time to File for Trello
				Trello.recordEndTime(); 
			} 
			catch (IOException e) { e.printStackTrace(); }
			
			// End Program
			System.exit(0);
		}
	}
	
	public static void main(String[] args) 
	{
		new Samaritan();
		return;
	}

	private class TwitchCommand implements IChatCommand
	{
		@Override
		public boolean execute(CommandPacket command, CommandResults results) 
		{
			ArrayList<Integer> twitchResult = null;
			try 
			{
				twitchResult = Twitch.check(channelID, TwitchNames, TwitchURLs);
			} 
			catch (ParseException | IOException | InterruptedException e1) { e1.printStackTrace(); }
			
			if (twitchResult.size() == 0) 
			{
				String[] twitchInfo = {"``[Twitch]`` No Pixeltail developers are currently streaming."};
				
				try 
				{
					DiscordHelper.write(channelID, twitchInfo);
				} 
				catch (IOException | InterruptedException e) { e.printStackTrace(); }
			}
			return true;
		}

		@Override
		public String getDescriptionString() 
		{
			// TODO Auto-generated method stub
			return "Prints a list of streaming twitch channels";
		}
	}
}